package pets;

public class Elephant extends Pet{

	public Elephant(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Toot!!!");
		return "Toot";
	}	
}
