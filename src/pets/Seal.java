package pets;

public class Seal extends Pet{

	public Seal(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("OW OW OW!!!");
		return "OW OW OW";
	}	
}
