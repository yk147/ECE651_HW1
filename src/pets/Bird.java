package pets;

public class Bird extends Pet{

	public Bird(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Tweet!!!");
		return "Tweet";
	}	
}
