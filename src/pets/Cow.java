package pets;

public class Cow extends Pet{

	public Cow(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Moo!!!");
		return "Moo";
	}	
}
