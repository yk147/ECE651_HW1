package pets;

import java.util.Random;

public class Fox extends Pet{
	private String[] noises = new String[5];

	public Fox(String name) {
		super(name);
	}
	
	private void populateNoises(){
		noises[0] = "Ring-ding-ding-ding-dingeringeding!";
		noises[1] = "Gering-ding-ding-ding-dingeringeding!";
		noises[2] = "Wa-pa-pa-pa-pa-pa-pow!";
		noises[3] = "Hatee-hatee-hatee-ho!";
		noises[4] = "Joff-tchoff-tchoff-tchoffo-tchoffo-tchoff!";
	}

	@Override
	public String makeNoise() {
		this.populateNoises();
		Random rand = new Random();
		int randomNum = rand.nextInt((3-0)+1) + 0;
		System.out.println(noises[randomNum]);
		return noises[randomNum];
	}	
}
