package pets;

public class Cat extends Pet{

	public Cat(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Meow!!!");
		return "Meow";
	}	
}
