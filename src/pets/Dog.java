package pets;

public class Dog extends Pet{

	public Dog(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Woof!!!");
		return "Woof";
	}	
}
