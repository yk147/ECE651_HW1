package pets;

public class Duck extends Pet{

	public Duck(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Quack!!!");
		return "Quack";
	}	
}
