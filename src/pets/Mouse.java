package pets;

public class Mouse extends Pet{

	public Mouse(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Squeak!!!");
		return "Squeak";
	}	
}
