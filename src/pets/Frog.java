package pets;

public class Frog extends Pet{

	public Frog(String name) {
		super(name);
	}

	@Override
	public String makeNoise() {
		System.out.println("Croak!!!");
		return "Croak";
	}	
}
