import java.awt.*;
import java.awt.event.*;
import pets.*;


import javax.swing.*;

public class GUI extends JFrame{
	public static final String DEFAULT_TEXT = "What does the pet say?";
	private JTextField myDisplay;
	private JButton Dog, Cat, Bird, Mouse, Cow, Frog, Elephant, Duck, Seal, Fox;
	private String myText;
	private GridLayout myGrid;
	private JPanel contentPanel;
	private JPanel displayPanel;
	
	
	/*
	 * LIST OF ALL PETS
	 * NOTE THAT WE ARE ONLY USING THE MAKENOISE METHOD 
	 * (Actual name of pet is not important for this program)
	 */
	
	private Dog dog = new Dog("dog");
	private Cat cat = new Cat("cat");
	private Bird bird = new Bird("bird");
	private Mouse mouse = new Mouse("mouse");
	private Cow cow = new Cow ("cow");
	private Frog frog = new Frog ("frog");
	private Elephant elephant = new Elephant ("elephant");
	private Duck duck = new Duck ("duck");
	private Seal seal = new Seal ("seal");
	private Fox fox = new Fox("fox");
	
	public GUI(){
		super("ECE651 HW1");
		
		Font displayFont = new Font("SansSerif", Font.BOLD, 12);
		
		myDisplay = new JTextField(null,25);
		myDisplay.setEditable(false);
		myDisplay.setFont(displayFont);
		myDisplay.setHorizontalAlignment(JTextField.CENTER);
		myDisplay.setBackground(Color.lightGray);
		myDisplay.setText(DEFAULT_TEXT);
		
		Dog = new JButton("Dog");
		Cat = new JButton("Cat");
		Bird = new JButton("Bird");
		Mouse = new JButton("Mouse");
		Cow = new JButton("Cow");
		Frog = new JButton("Frog");
		Elephant = new JButton("Elephant");
		Duck = new JButton("Duck");
		Seal = new JButton("Seal");
		Fox = new JButton("Fox");
		
		/*
		 * Setting up Action Listeners 
		 */
		PetSounds p = new PetSounds();
		Dog.addActionListener(p);	Cat.addActionListener(p);
		Bird.addActionListener(p);	Mouse.addActionListener(p);
		Cow.addActionListener(p);	Frog.addActionListener(p);
		Elephant.addActionListener(p);	Duck.addActionListener(p);
		Seal.addActionListener(p);	Fox.addActionListener(p);
		
		
		/*
		 * ADD CONTENT TO GUI
		 */

		GridLayout myGrid = new GridLayout(0,2);
		displayPanel = new JPanel();
		displayPanel.setLayout(new GridLayout(2,1,10,10));
		displayPanel.setBackground(Color.LIGHT_GRAY);
		displayPanel.add(myDisplay);
		contentPanel = new JPanel();
		contentPanel.setLayout(myGrid);
		contentPanel.add(Dog); 		contentPanel.add(Cat);
		contentPanel.add(Bird);		contentPanel.add(Mouse);
		contentPanel.add(Cow);		contentPanel.add(Frog);
		contentPanel.add(Elephant);	contentPanel.add(Duck);
		contentPanel.add(Seal);		contentPanel.add(Fox);
		displayPanel.add(contentPanel);
		this.setContentPane(displayPanel);
	}
	
	private class PetSounds implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			String output;
			JButton src = (JButton) event.getSource();
			
			if(src.equals(Dog)){
				output = dog.makeNoise();
				myDisplay.setText("The Dog goes " + output);
			}
			if(src.equals(Cat)){
				output = cat.makeNoise();
				myDisplay.setText("The Cat goes " + output);
			}
			if(src.equals(Bird)){
				output = bird.makeNoise();
				myDisplay.setText("The Bird goes " + output);
			}
			if(src.equals(Mouse)){
				output = mouse.makeNoise();
				myDisplay.setText("The Mouse goes " + output);
			}
			if(src.equals(Cow)){
				output = cow.makeNoise();
				myDisplay.setText("The Cow goes " + output);
			}
			if(src.equals(Frog)){
				output = frog.makeNoise();
				myDisplay.setText("The Frog goes " + output);
			}
			if(src.equals(Elephant)){
				output = elephant.makeNoise();
				myDisplay.setText("The Elephant goes " + output);
			}
			if(src.equals(Duck)){
				output = duck.makeNoise();
				myDisplay.setText("The Duck goes " + output);
			}
			if(src.equals(Seal)){
				output = seal.makeNoise();
				myDisplay.setText("The Seal goes " + output);
			}
			if(src.equals(Fox)){
				output = fox.makeNoise();
				myDisplay.setText("The Fox goes " + output);
			}
		}
		
	}
	
}
